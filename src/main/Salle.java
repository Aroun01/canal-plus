package main;

import Exception.CapaciteException;
import Exception.DateException;
import Exception.IntervalException;
import Exception.SalleException;
import Outil.Interval;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Salle {

    private Integer capacity;
    private String nom;
    private Map<Integer,List<String>> equipementsList;
    private Map<Interval,Boolean> crenaux= new HashMap<>();

    public Map<Interval, Boolean> getCrenaux() {
        return crenaux;
    }
    public Map<Integer,List<String>>getEquipementsList() {
        return equipementsList;
    }



    public Salle(Integer capacity, String nom, Map<Integer, List<String>> equipementsList, Map<Interval, Boolean> crenaux) {
        this.capacity = capacity;
        this.nom = nom;
        this.equipementsList = equipementsList;
        this.crenaux = crenaux;
    }

    public void reserver(int start , String date , Integer nbrPersonne) throws ParseException, DateException, CapaciteException, SalleException, IntervalException {

        Calendar mydate = new GregorianCalendar();
        Date thedate = new SimpleDateFormat("dd-MM-yyyy", Locale.FRENCH).parse(date);
        mydate.setTime(thedate);

         if(nbrPersonne < (capacity * 70) / 100) {

             if ((mydate.get(Calendar.DAY_OF_WEEK) != 6 &&  mydate.get(Calendar.DAY_OF_WEEK) != 7 && start>=8 && start<=19) ) {

                 for (Map.Entry<Interval, Boolean> e : crenaux.entrySet()) {

                     if (e.getKey().getStart() == start ) {
                         if (e.getValue()) {
                             e.setValue(false);
                             if (start < 19) {
                                 getNextFreeIntervalMap(crenaux, start);
                             }

                             if (start > 8) {
                                 getPrevFreeIntervalMap(crenaux, start);
                             }

                         }
                         else{
                             throw new SalleException("Salle indisponible pour ce crénau !");


                         }
                     }



                 }
             } else {

                 throw new DateException("Format de Date ou heure non valide non valide ! veuillez respectez un format du type dd/MM/yyyy et assurez vous que le jour n'est pas un Samedi ou Dimanche et l'heure est comprise entre 8h et 20h!");

             }
         }
         else {
             throw new CapaciteException("Capacité insupportable pour cette salle !");

         }

    }

    public void getNextFreeIntervalMap(Map<Interval, Boolean> crenaux, int start) {
       int next=start+1;

        for (Map.Entry<Interval, Boolean> e : crenaux.entrySet()) {
            if (e.getKey().getStart() == next && e.getValue()) {
                e.setValue(false);
            }
        }

    }

    public void getPrevFreeIntervalMap(Map<Interval, Boolean> crenaux, int start) {
        int prev=start-1;

        for (Map.Entry<Interval, Boolean> e : crenaux.entrySet()) {
            if (e.getKey().getStart() == prev && e.getValue()) {
                e.setValue(false);
            }
        }

    }


}

