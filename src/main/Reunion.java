package main;

import Exception.*;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Reunion {

    private String numRunion;
    private String sujetReunion;
    private int HeureReunion;
    private String DateRunion;
    private String typeReunion;
    private Integer nbrPersonnes;

    public Reunion(String numRunion, String sujetReunion, int heureReunion, String dateRunion, String typeReunion, Integer nbrPersonnes) {
        this.numRunion = numRunion;
        this.sujetReunion = sujetReunion;
        HeureReunion = heureReunion;
        DateRunion = dateRunion;
        this.typeReunion = typeReunion;
        this.nbrPersonnes = nbrPersonnes;
    }

    public void programmerRunion(Salle salle) throws ParseException, CapaciteException, DateException, SalleException, SalleSansMaterialException, IntervalException {


        salle.reserver(HeureReunion, DateRunion, nbrPersonnes);

        switch (typeReunion) {

            case "VC":
                List<String> equipementsVc = Stream.of("Ecran", "Pieuvre", "Webcam").collect(Collectors.toList());
                if (salle.getEquipementsList().entrySet().stream().filter(e -> e.getKey() >= nbrPersonnes && e.getValue().containsAll(equipementsVc)).findFirst().isPresent()) {
                    System.out.print("Salle VC réservé avec success");
                } else {
                    throw new SalleSansMaterialException("La salle ne dispose pas d'assez d'equipements !");
                }

                break;

            case "SPEC":
                List<String> equipementsSpec = Stream.of("Tableau").collect(Collectors.toList());
                if (salle.getEquipementsList().entrySet().stream().filter(e -> e.getKey() >= nbrPersonnes && e.getValue().containsAll(equipementsSpec)).findFirst().isPresent()) {
                    System.out.print("Salle SPEC réservé avec success");
                } else {
                    throw new SalleSansMaterialException("La salle ne dispose pas d'assez d'equipements !");
                }

                break;

            case "RS":
                if (nbrPersonnes > 3) {
                    System.out.print("Salle RS réservé avec success");
                }

                break;

            case "RC":
                List<String> equipementsRc = Stream.of("Ecran", "Pieuvre", "Tableau").collect(Collectors.toList());
                if (salle.getEquipementsList().entrySet().stream().filter(e -> e.getKey() >= nbrPersonnes && e.getValue().containsAll(equipementsRc)).findFirst().isPresent()) {
                    System.out.print("Salle RC réservé avec success");
                } else {
                    throw new SalleSansMaterialException("La salle ne dispose pas d'assez d'equipements !");
                }

                break;


        }


    }


}

