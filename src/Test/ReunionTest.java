package Test;

import Exception.*;
import Outil.Interval;
import main.Reunion;
import main.Salle;
import org.junit.*;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReunionTest {

    @Test
    public void reserverSalle() throws IntervalException, ParseException, CapaciteException, DateException, SalleException, SalleSansMaterialException {

        List<String> equipementsVc = Stream.of("Ecran", "Pieuvre", "Webcam").collect(Collectors.toList());


        Map<Integer, List<String>> equipementsList = Stream.of(
                new AbstractMap.SimpleEntry<>(13, equipementsVc))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<Interval, Boolean> crenaux = new HashMap<Interval, Boolean>() {{
            put(new Interval(8, 9), true);
            put(new Interval(9, 10), true);
            put(new Interval(10, 11), true);
            put(new Interval(11, 12), true);
            put(new Interval(12, 13), true);
            put(new Interval(13, 14), true);
            put(new Interval(14, 15), true);
            put(new Interval(15, 16), true);
            put(new Interval(16, 17), true);
            put(new Interval(17, 18), true);
            put(new Interval(18, 19), true);
            put(new Interval(19, 20), true);


        }};


        Salle salle = new Salle(20, "E3001", equipementsList, crenaux);
        Reunion reunion = new Reunion("1", "MEP", 9, "21-01-2021", "VC", 5);

        reunion.programmerRunion(salle);

        Assert.assertFalse(salle.getCrenaux().entrySet().stream().filter(e -> e.getKey().getStart() == 10).map(Map.Entry::getValue).collect(Collectors.toList()).get(0));
        Assert.assertTrue(salle.getCrenaux().entrySet().stream().filter(e -> e.getKey().getStart() == 11).map(Map.Entry::getValue).collect(Collectors.toList()).get(0));
        Assert.assertFalse(salle.getCrenaux().entrySet().stream().filter(e -> e.getKey().getStart() == 8).map(Map.Entry::getValue).collect(Collectors.toList()).get(0));


    }


    @Test(expected = DateException.class)
    public void dateInvalideTest() throws IntervalException, ParseException, CapaciteException, DateException, SalleException, SalleSansMaterialException {
        List<String> equipementsVc = Stream.of("Ecran", "Pieuvre", "Webcam").collect(Collectors.toList());


        Map<Integer, List<String>> equipementsList = Stream.of(
                new AbstractMap.SimpleEntry<>(13, equipementsVc))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<Interval, Boolean> crenaux = new HashMap<Interval, Boolean>() {{
            put(new Interval(8, 9), true);
            put(new Interval(9, 10), true);
            put(new Interval(10, 11), true);
            put(new Interval(11, 12), true);
            put(new Interval(12, 13), true);
            put(new Interval(13, 14), true);
            put(new Interval(14, 15), true);
            put(new Interval(15, 16), true);
            put(new Interval(16, 17), true);
            put(new Interval(17, 18), true);
            put(new Interval(18, 19), true);
            put(new Interval(19, 20), true);


        }};
        Salle salle = new Salle(20, "E3001", equipementsList, crenaux);

        Reunion reunion = new Reunion("1", "MEP", 9, "30-01-2021", "VC", 5);

        reunion.programmerRunion(salle);
    }

    @Test(expected = CapaciteException.class)
    public void capaciteTest() throws IntervalException, ParseException, CapaciteException, DateException, SalleException, SalleSansMaterialException {
        List<String> equipementsVc = Stream.of("Ecran", "Pieuvre", "Webcam").collect(Collectors.toList());


        Map<Integer, List<String>> equipementsList = Stream.of(
                new AbstractMap.SimpleEntry<>(13, equipementsVc))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<Interval, Boolean> crenaux = new HashMap<Interval, Boolean>() {{
            put(new Interval(8, 9), true);
            put(new Interval(9, 10), true);
            put(new Interval(10, 11), true);
            put(new Interval(11, 12), true);
            put(new Interval(12, 13), true);
            put(new Interval(13, 14), true);
            put(new Interval(14, 15), true);
            put(new Interval(15, 16), true);
            put(new Interval(16, 17), true);
            put(new Interval(17, 18), true);
            put(new Interval(18, 19), true);
            put(new Interval(19, 20), true);


        }};
        Salle salle = new Salle(10, "E3001", equipementsList, crenaux);

        Reunion reunion = new Reunion("1", "MEP", 9, "21-01-2021", "VC", 30);

        reunion.programmerRunion(salle);
    }


    @Test(expected = SalleException.class)
    public void reserverSalleIndisponible() throws IntervalException, ParseException, CapaciteException, DateException, SalleException, SalleSansMaterialException {

        List<String> equipementsVc = Stream.of("Ecran", "Pieuvre", "Webcam").collect(Collectors.toList());


        Map<Integer, List<String>> equipementsList = Stream.of(
                new AbstractMap.SimpleEntry<>(13, equipementsVc))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<Interval, Boolean> crenaux = new HashMap<Interval, Boolean>() {{
            put(new Interval(8, 9), true);
            put(new Interval(9, 10), true);
            put(new Interval(10, 11), true);
            put(new Interval(11, 12), true);
            put(new Interval(12, 13), true);
            put(new Interval(13, 14), true);
            put(new Interval(14, 15), true);
            put(new Interval(15, 16), true);
            put(new Interval(16, 17), true);
            put(new Interval(17, 18), true);
            put(new Interval(18, 19), true);
            put(new Interval(19, 20), true);


        }};


        Salle salle = new Salle(20, "E3001", equipementsList, crenaux);
        Reunion reunion = new Reunion("1", "MEP", 9, "21-01-2021", "VC", 5);
        Reunion reunion2 = new Reunion("1", "MEP", 10, "21-01-2021", "VC", 5);

        reunion.programmerRunion(salle);
        reunion2.programmerRunion(salle);

    }


    @Test(expected = SalleSansMaterialException.class)
    public void reserverSalleSansMaterial() throws IntervalException, ParseException, CapaciteException, DateException, SalleException, SalleSansMaterialException {

        List<String> equipementsVc = Stream.of("Ecran", "Pieuvre").collect(Collectors.toList());


        Map<Integer, List<String>> equipementsList = Stream.of(
                new AbstractMap.SimpleEntry<>(13, equipementsVc))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<Interval, Boolean> crenaux = new HashMap<Interval, Boolean>() {{
            put(new Interval(8, 9), true);
            put(new Interval(9, 10), true);
            put(new Interval(10, 11), true);
            put(new Interval(11, 12), true);
            put(new Interval(12, 13), true);
            put(new Interval(13, 14), true);
            put(new Interval(14, 15), true);
            put(new Interval(15, 16), true);
            put(new Interval(16, 17), true);
            put(new Interval(17, 18), true);
            put(new Interval(18, 19), true);
            put(new Interval(19, 20), true);


        }};
        Salle salle = new Salle(20, "E3001", equipementsList, crenaux);
        Reunion reunion = new Reunion("1", "MEP", 9, "21-01-2021", "VC", 5);

        reunion.programmerRunion(salle);


    }

    @Test(expected = DateException.class)
    public void reserverhorsPlageHorraire() throws IntervalException, ParseException, CapaciteException, DateException, SalleException, SalleSansMaterialException {

        List<String> equipementsVc = Stream.of("Ecran", "Pieuvre", "Webcam").collect(Collectors.toList());


        Map<Integer, List<String>> equipementsList = Stream.of(
                new AbstractMap.SimpleEntry<>(13, equipementsVc))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<Interval, Boolean> crenaux = new HashMap<Interval, Boolean>() {{
            put(new Interval(8, 9), true);
            put(new Interval(9, 10), true);
            put(new Interval(10, 11), true);
            put(new Interval(11, 12), true);
            put(new Interval(12, 13), true);
            put(new Interval(13, 14), true);
            put(new Interval(14, 15), true);
            put(new Interval(15, 16), true);
            put(new Interval(16, 17), true);
            put(new Interval(17, 18), true);
            put(new Interval(18, 19), true);
            put(new Interval(19, 20), true);


        }};
        Salle salle = new Salle(20, "E3001", equipementsList, crenaux);
        Reunion reunion = new Reunion("1", "MEP", 7, "21-01-2021", "VC", 5);

        reunion.programmerRunion(salle);
    }
}