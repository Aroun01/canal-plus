package Outil;

import Exception.IntervalException;

public class Interval implements Comparable {
    int start,end;
    public Interval(int start, int end)  throws IntervalException {
        if(start>=8 && start<=19 ){
            this.start=start;
            this.end=start+1;
        }
        else {
            throw new IntervalException("Crénaux horaire non valide");
        }

    }

    public int getStart() {
        return start;
    }

    @Override
    public int compareTo(Object o) {

        Interval tmp = (Interval)o;

        if(this.start==tmp.start){
            return 0;
        }


        return 1;
    }

}